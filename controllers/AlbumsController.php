<?php

namespace app\controllers;

use Yii;
use app\models\Albums;
use app\models\AlbumsSearch;
use app\models\ArtistsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlbumsController implements the CRUD actions for Albums model.
 */
class AlbumsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Albums models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ( Yii::$app->user->isGuest ){
    		return $this->redirect(['/site/login']);
    	}else{
    		$searchModel = new AlbumsSearch();
		    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		    return $this->render('index', [
		        'searchModel' => $searchModel,
		        'dataProvider' => $dataProvider,
		    ]);
    	}
    }

    /**
     * Displays a single Albums model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
    	if ( Yii::$app->user->isGuest ){
    		return $this->redirect(['/site/login']);
    	}else{
		    return $this->render('view', [
		        'model' => $this->findModel($id),
		    ]);
		}
    }


    public function actionCreate()
    {
    
    	if ( Yii::$app->user->isGuest ){
    		return $this->redirect(['/site/login']);
    	}else{
		    $model = new Albums();
			$searchModel = new ArtistsSearch();
		    $dataProvider = $searchModel->getAll();
		    
		    if ($model->load(Yii::$app->request->post())) {
		    	
		    	if($model->save()){
		        	return $this->redirect(['index']);
		        }
		    }
		    
		    
		    $query= array();
		    foreach( $dataProvider->query as $data ){
		    	$query[$data["id"]]= $data["artist_name"];
		    }
			

		    return $this->render('create', [
		        'model' => $model,
		        'artists' => $query,
		    ]);
		}
    }

    /**
     * Updates an existing Albums model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
    	if ( Yii::$app->user->isGuest ){
    		return $this->redirect(['/site/login']);
    	}else{
		    $model = $this->findModel($id);
		    $searchModel = new ArtistsSearch();
		    $dataProvider = $searchModel->getAll();

		    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		        return $this->redirect(['view', 'id' => $model->id]);
		    }

		    $query= array();
		    foreach( $dataProvider->query as $data ){
		    	$query[$data["id"]]= $data["artist_name"];
		    }
			

		    return $this->render('update', [
		        'model' => $model,
		        'artists' => $query,
		    ]);
		}
    }

    /**
     * Deletes an existing Albums model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
    	if ( Yii::$app->user->isGuest ){
    		return $this->redirect(['/site/login']);
    	}else{
		    $this->findModel($id)->delete();

		    return $this->redirect(['index']);
		}
    }

    /**
     * Finds the Albums model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Albums the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
    	if ( Yii::$app->user->isGuest ){
    		return $this->redirect(['/site/login']);
    	}else{
		    if (($model = Albums::findOne($id)) !== null) {
		        return $model;
		    }

		    throw new NotFoundHttpException('The requested page does not exist.');
		}
    }
}
