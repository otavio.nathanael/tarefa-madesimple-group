## First things first

I am using composer for managing my dependencies. When commiting the application to git, the vendor folder is ignored. Before everything else,
create the "vendor" directory and run the command:

~~~
composer update
~~~

If you don't have composer on your machine, use the local one that I commited. In order to use it run:

~~~
php composer.phar update
~~~

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

After editing the database configurations, inside the project root directory, run the command below on terminal to migrate the tables to database:

~~~
php yii migrate
~~~



**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- The command migrate may fail if php client is not up to date or if some php module is missing for some reason.
- The database used during tests was mysql from xampp, and so indexation is ommited from the migration code since mysql from xampp does not require it. I looked on google and saw that this command may fail if using postgresql.

Running the Application
-------------

The best way to get the application up and running is using the command on terminal in the root directory of the project:

~~~
php yii serve
~~~

It will start the built-in server on localhost:8080.

I'd recommend using the buit-in server for testing the application since Yii2 is quite tricky when it comes to deploying the application for production on a real world server. 
