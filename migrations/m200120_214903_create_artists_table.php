<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%artists}}`.
 */
class m200120_214903_create_artists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%artists}}', [
            'id' => $this->primaryKey(),
            'artist_name' => $this->string()->notNull(),
            'twiter_handle' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%artists}}');
    }
}
