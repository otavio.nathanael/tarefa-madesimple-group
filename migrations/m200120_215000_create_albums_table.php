<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%albums}}`.
 */
class m200120_215000_create_albums_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%albums}}', [
            'id' => $this->primaryKey(),
            'album_name' => $this->string()->notNull(),
            'year' => $this->string()->notNull(),
            'artist_id' => $this->integer()->notNull(),
        ]);
        
         $this->addForeignKey(
            '{{%fk-albums-artist_id}}',
            '{{%albums}}',
            'artist_id',
            '{{%artists}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%albums}}');
    }
}
