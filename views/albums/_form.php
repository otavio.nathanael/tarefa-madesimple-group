<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Albums */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="albums-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'album_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year')->widget(\yii\widgets\MaskedInput::className(), [
    	'name' => 'year',
    	'mask' => '9999'
    ]) ?>

    <?= $form->field($model, 'artist_id')->dropDownList($artists)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
